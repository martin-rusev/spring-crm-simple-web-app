package spring.crm.webapp.repositories.contracts;

import spring.crm.webapp.models.Customer;

import java.util.List;

public interface CustomerDAO {
    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

    Customer getCustomer(int id);

    void deleteCustomer(Customer customer);

    List<Customer> findCustomers(String name);
}
