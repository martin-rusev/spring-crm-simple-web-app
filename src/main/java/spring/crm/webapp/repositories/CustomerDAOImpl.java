package spring.crm.webapp.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.crm.webapp.models.Customer;
import spring.crm.webapp.repositories.contracts.CustomerDAO;

import java.util.List;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Customer> getCustomers() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Customer", Customer.class)
                    .list();
        }
    }

    @Override
    public void saveCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(customer);
            session.getTransaction().commit();
        }
    }

    @Override
    public Customer getCustomer(int id) {
        try (Session session = sessionFactory.openSession()) {
            Customer customer = session.get(Customer.class, id);
            return customer;
        }
    }

    @Override
    public void deleteCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(customer);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Customer> findCustomers(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer where lower(firstName) like :name or lower(lastName) like :name", Customer.class);
            query.setParameter("name", "%" + name.toLowerCase() + "%");
            return query.list();
        }
    }
}
