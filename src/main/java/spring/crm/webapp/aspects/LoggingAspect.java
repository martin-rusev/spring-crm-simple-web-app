package spring.crm.webapp.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Aspect
@Component
public class LoggingAspect {
    private static final Logger logger = Logger.getLogger(LoggingAspect.class.getName());

    @Pointcut("execution(* spring.crm.webapp.controllers.*.*(..))")
    public void forControllerPackage() {
    }

    @Pointcut("execution(* spring.crm.webapp.services.*.*(..))")
    public void forServicePackage() {
    }

    @Pointcut("execution(* spring.crm.webapp.repositories.*.*(..))")
    public void forRepositoryPackage() {
    }

    @Pointcut("forControllerPackage() || forServicePackage() || forRepositoryPackage()")
    public void forAppFlow() {
    }

    @Before("forAppFlow()")
    public void beforeAdvice(JoinPoint joinPoint) {

        String method = joinPoint.getSignature().toShortString();

        logger.info("============= Before -->> Calling method: " + method);

        Object[] args = joinPoint.getArgs();

        for (Object argument : args) {
            logger.info("Method argument: " + argument);
        }

        System.out.println("Print Check");
    }

    @AfterReturning(pointcut = "forAppFlow()",
                    returning = "result")
    public void afterReturningAdvice(JoinPoint joinPoint, Object result) {

        String method = joinPoint.toShortString();

        logger.info("======AfterReturning Advice-----> Called method: " + method);

        logger.info("Returned object: " + result);

    }
}
