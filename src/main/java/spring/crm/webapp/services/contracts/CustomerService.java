package spring.crm.webapp.services.contracts;

import spring.crm.webapp.models.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

    Customer getCustomer(int id);

    void deleteCustomer(Customer customer);

    List<Customer> findCustomers(String name);
}
