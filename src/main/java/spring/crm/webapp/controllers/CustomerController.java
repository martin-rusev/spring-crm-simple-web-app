package spring.crm.webapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import spring.crm.webapp.services.contracts.CustomerService;
import spring.crm.webapp.models.Customer;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping()
    public String getCustomers(Model model) {
        List<Customer> customers = customerService.getCustomers();

        model.addAttribute("customers", customers);

        return "list-customers";
    }

    @GetMapping("/add-customer")
    public String showForm(Model model) {
        model.addAttribute("customer", new Customer());

        return "customer-form";
    }

    @PostMapping("/add-customer")
    public String addCustomer(@Valid @ModelAttribute(name = "customer") Customer customer,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "customer-form";
        }
        customerService.saveCustomer(customer);

        return "redirect:/customers";
    }

    @GetMapping("/update-customer")
    public String updateCustomer(@RequestParam(name = "customerId") int id, Model model) {
        Customer customer = customerService.getCustomer(id);

        model.addAttribute("customer", customer);

        return "customer-form";
    }

    @GetMapping("/delete-customer")
    public String deleteCustomer(@RequestParam(name = "customerId") int id, Model model) {
        Customer customer = customerService.getCustomer(id);

        customerService.deleteCustomer(customer);

        return "redirect:/customers";
    }

    @GetMapping("/search")
    public String findCustomers(@RequestParam("name") String name,
                                  Model model) {
        List<Customer> customers = customerService.findCustomers(name);

        model.addAttribute("customers", customers);

        return "list-customers";
    }
}
